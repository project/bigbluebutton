<?php

declare(strict_types=1);

namespace Drupal\bigbluebutton\Controller;

use Drupal\bigbluebutton\BigBlueButtonHelper;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Big Blue Button routes.
 */
final class BigBlueButtonMeetingEndController extends ControllerBase {

  /**
   * The controller constructor.
   */
  public function __construct(
    private readonly BigBlueButtonHelper $helper,
    private readonly RouteMatchInterface $routeMatch,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('bigbluebutton.helper'),
      $container->get('current_route_match')
    );
  }

  /**
   * Builds the response.
   */
  public function __invoke(string $meeting_id, string $meeting_type): array {

    \Drupal::logger('bigbluebutton')->debug('The Meeting End Callback was triggered - Meeting ID: ' . $meeting_id . ' Meeting type: ' . $meeting_type);

    $this->helper->setActionOnMeetingEndCallback($meeting_id, $meeting_type);

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('Thank you!'),
    ];

    return $build;

  }

}
