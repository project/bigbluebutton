<?php

namespace Drupal\bigbluebutton\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\bigbluebutton\BBB;
use BigBlueButton\Parameters\GetRecordingsParameters;

/**
 * Defines Virtual Event BBB Recording Controller.
 */
class BigBlueButtonRecordingController extends ControllerBase {

  /**
   * Viewrecording.
   *
   * @return string
   *   Return Hello string.
   */
  public function viewRecording($entity_uuid, $recording_id) {

    // Define all supported recording format types.
    $supported_format_types = [
      'video_mp4',
      'presentation',
      'screenshare'
    ];

    $user = \Drupal::currentUser();

    $apiUrl = \Drupal::config('bigbluebutton.settings')->get('hostname');
    $secretKey = \Drupal::config('bigbluebutton.settings')->get('secret');
    $bbb = new BBB($secretKey, $apiUrl);

    $recordingParams = new GetRecordingsParameters();
    $recordingParams->setMeetingID($entity_uuid);

    try {
      // Get recordings.
      $response = $bbb->getRecordings($recordingParams);
      $recordings = [];
      foreach ($response->getRawXml()->recordings->recording as $key => $recording){
        foreach ($recording->playback as $key => $playback){
          foreach ($recording->playback->format as $key => $format){
            if (in_array($format->type, $supported_format_types)) {
              $format->recordID = $recording->recordID;
              $recordings[] = $format;
            }
          }
        }
      }

      $recording = "";
      foreach ($recordings as $key => $value) {
        if ($value->recordID == $recording_id) {
          $recording = $value;
          break;
        }
      }

      if ($recording->type == "presentation") {
        return [
          '#theme' => 'bbb_recordings_video_iframe',
          '#url' => $recording->url,
        ];
      }
      else {
        return [
          '#theme' => 'bbb_recordings_video_video',
          '#url' => $recording->url,
        ];
      }
    }
    catch (\RuntimeException $exception) {
      $this->getLogger('bigblueblutton')->warning($exception->getMessage());
      $error_message = $this->t("Couldn't get recordings! please contact system administrator.");
      $this->messenger()->addError($error_message);
    }
  }

}