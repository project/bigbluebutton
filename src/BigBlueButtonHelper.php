<?php declare(strict_types = 1);

namespace Drupal\bigbluebutton;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\bigbluebutton\BBB;
use BigBlueButton\Parameters\GetMeetingInfoParameters;
use BigBlueButton\Parameters\CreateMeetingParameters;
use BigBlueButton\Parameters\HooksCreateParameters;
use BigBlueButton\Parameters\JoinMeetingParameters;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Url;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Utility\BytesValidator;
use Drupal\Component\Utility\Bytes;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Database\DatabaseException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * @todo Add class description.
 */
final class BigBlueButtonHelper {

  use MessengerTrait;
  use LoggerChannelTrait;
  use StringTranslationTrait;

  const FAILURE_NO_ENTITY = 1;
  const FAILURE_NO_VALID_HOST_OR_SECRET = 2;
  const FAILURE_NO_MEETING_DATA = 3;
  

  /**
   * Constructs a BigBlueButtonHelpper object.
   */
  public function __construct(
    private readonly AccountProxyInterface $currentUser,
    private readonly EntityTypeManagerInterface $entityTypeManager,
    private readonly ConfigFactoryInterface $configFactory,
    private readonly EntityFieldManagerInterface $entityFieldManager,
    private readonly FileUrlGeneratorInterface $fileUrlGenerator,
    private readonly ModuleHandlerInterface $moduleHandler,
    private readonly EntityRepositoryInterface $entityRepository,
  ) {}


  /**
   * Get the BBB host.
   *
   * @return 
   *   Returns a qualified BBB host.
   */
  protected function getBBBHost() {
    $host = $this->configFactory->get('bigbluebutton.settings')->get('hostname');
    return $host;
  }

  /**
   * Get the BBB Secret.
   *
   * @return 
   *   Returns the qualified BBB secret.
   */
  protected function getBBBSecret() {
    $secret = $this->configFactory->get('bigbluebutton.settings')->get('secret');
    return $secret;
  }

  /**
   * Get the BBB Presentation default.
   *
   * @return 
   *   Returns the presentation default.
   */
  protected function getPresentationDefault() {
    $presentation_default = $this->configFactory->get('bigbluebutton.settings')->get('presentation_default');
    return $presentation_default;
  }

  public function prepareCreateMeeting(ContentEntityInterface $entity) {

    // First let's get the BBB Field


    $fields = $this->entityFieldManager
      ->getFieldDefinitions($entity->getEntityTypeId(), 
                            $entity->bundle());

    // As there should always be one BBB Field per entity only
    // we can savely loop through our fields. 
    foreach ($fields as $key => $field) {
      if ($field->getType() === 'bigbluebutton') {  
       $field_name = $field->getName();
      }
    }

    // If we have a field_name of type 'bigbluebutton'
    // We want to create our meeting, if some body enabled it.
    if (isset($field_name)) {     

      // We only want to create a meeting
      // if we have enabled BBB functionality.
      if (!$entity->{$field_name}->enabled) {
        return;
      }

      $apiUrl = $this->getBBBHost();
      $secretKey = $this->getBBBSecret();
      $bbb = new BBB($secretKey, $apiUrl);
 
      $createMeetingParams = new CreateMeetingParameters($entity->uuid(), $entity->label());
 
 
      try {
        $response = $bbb->createMeeting($createMeetingParams);
        if ($response->getReturnCode() == 'FAILED') {
          $error_message = t("Couldn't create room! please contact system administrator.");
          $this->messenger()->addError($error_message);
        }
        else {
          $entity->{$field_name}->attendeePW = $response->getAttendeePassword();
          $entity->{$field_name}->moderatorPW = $response->getModeratorPassword();
        }
      }
      catch (\RuntimeException $exception) {
        $this->getLogger('bigbluebutton')->warning($exception->getMessage());
        $error_message = t("Couldn't create room! please contact system administrator.");
        $this->messenger()->addError($error_message);
      }
      catch (\Exception $exception) {
        $this->getLogger('bigbluebutton')->warning($exception->getMessage());
        $error_message = t("Couldn't create room! please contact system administrator.");
        $this->messenger()->addError($error_message);
      }
    }
  }

  public function getBBBFieldFromEntity($entity) {

    $bbb_field = FALSE;
    
    $fields = $this->entityFieldManager
      ->getFieldDefinitions($entity->getEntityTypeId(), 
                            $entity->bundle());

    // As there should always be one BBB Field per entity only
    // we can savely loop through our fields. 
    foreach ($fields as $key => $field) {
      if ($field->getType() === 'bigbluebutton') {  
       $field_name = $field->getName();
      }
    }

    // If we have a field_name of type 'bigbluebutton'
    // We want to create our meeting, if some body enabled it.
    if (isset($field_name)) {  
      $bbb_field = $entity->{$field_name};
    }

    return $bbb_field;

  }

  public function getMeetingInfo(string $entity_type_id, string $entity_uuid) {

    $data = [];

    if (isset($entity_uuid) && isset($entity_type_id)) {
      // Get Entity
      $entity = $this->getEntityFromUUID($entity_uuid, $entity_type_id);
    }

    if ($entity instanceof EntityInterface) {      

      $apiUrl = $this->getBBBHost();
      $secretKey = $this->getBBBSecret();
      $bbb = new BBB($secretKey, $apiUrl);

      // Read Moderator PW from BBB field
      $bbb_field = $this->getBBBFieldFromEntity($entity);
      // Get field machine name
      $bbb_field_name = $bbb_field->getName();
    
      // Check if meeting is not active,
      // recreate it before showing the join url
      $getMeetingInfoParams = new GetMeetingInfoParameters($entity->uuid(), $entity->{$bbb_field_name}->moderatorPW);

      try {
        $response = $bbb->getMeetingInfo($getMeetingInfoParams);

        \Drupal::logger('bigbluebutton')->notice('<pre>'.print_r($response, TRUE).'</pre>');

        if ($response->getReturnCode() == 'FAILED') {
          $data['state'] = 'IDLE';        
        }   
        else {
          $enc_response = json_encode($response->getRawXml());
          $dec_response = json_decode($enc_response, true);
          $data['state'] = 'RUNNING';  
          $data['bbb'] = $dec_response;
        }
        $data['drupal'] = [
          'entity_type_id' => $entity->getEntityTypeId(),
          'entity_id' => $entity->id(),
          'entity_uuid' => $entity->uuid()
        ];
      }
      catch (\RuntimeException $exception) {
        $data['state'] = 'ERROR';
        $this->getLogger('bigbluebutton')->warning($exception->getMessage());
        $error_message = $this->t("Couldn't get meeting info! please contact system administrator.");
        $this->messenger()->addError($error_message);
      }
    }

    return $data;

  }

  /**
   * @todo Add method description.
   */
  public function checkMeeting($entity) {

    $apiUrl = $this->getBBBHost();
    $secretKey = $this->getBBBSecret();
    $bbb = new BBB($secretKey, $apiUrl);

    // Read Moderator PW from BBB field
    $bbb_field = $this->getBBBFieldFromEntity($entity);
    // Get field machine name
    $bbb_field_name = $bbb_field->getName();
    
    // Check if meeting is not active,
    // recreate it before showing the join url
    $getMeetingInfoParams = new GetMeetingInfoParameters($entity->uuid(), $entity->{$bbb_field_name}->moderatorPW);

   try {
     $response = $bbb->getMeetingInfo($getMeetingInfoParams);
     if ($response->getReturnCode() == 'FAILED') {
       return FALSE;
     }   
   }
   catch (\RuntimeException $exception) {
     $this->getLogger('bigbluebutton')->warning($exception->getMessage());
     $error_message = $this->t("Couldn't get meeting info! please contact system administrator.");
     $this->messenger()->addError($error_message);
   }

   return TRUE;

  }

  /**
   * Undocumented function
   *
   * @param $entity
   *   The Entity object.
   * @return array $meeting_data
   *   The meeting id, moderator pw and attendee pw for the meeting.
   */
  public function createMeeting($entity): array {

    global $base_url;

    $meeting_data = [];

    $apiUrl = $this->getBBBHost();
    $secretKey = $this->getBBBSecret();

    // Check the given entity type
    $entity_type_id = $entity->getEntityTypeId();    

    if ($this->isValidExternalURL($apiUrl) && !empty($secretKey)) {
      
      $bbb = new BBB($secretKey, $apiUrl);

      // Get Read BBB field 
      $bbb_field = $this->getBBBFieldFromEntity($entity);
      
      $createMeetingParams = new CreateMeetingParameters($entity->uuid(), $entity->label());

      // Define Welcome message
      if (isset($bbb_field->welcome) && !empty($bbb_field->welcome)) {
        $welcome_message_value = $bbb_field->welcome;
        // Replace tokens if token module available only
        if ($this->tokenSupport()) {
          $welcome_message_value = \Drupal::token()->replace($bbb_field->welcome, [$entity_type_id => $entity]); 
        }       
        $createMeetingParams->setWelcomeMessage($welcome_message_value);
      }
      else {
        // BBB API using TRIM, but trim does not work on unset variables.
        $createMeetingParams->setWelcomeMessage('');
      }
      
      // Define moderator message
      if (isset($bbb_field->moderator_only_message) && !empty($bbb_field->moderator_only_message)) {
        $moderator_only_message_value = $bbb_field->moderator_only_message;
        // Replace tokens if token module available
        if ($this->tokenSupport()) { 
          $moderator_only_message_value = \Drupal::token()->replace($bbb_field->moderator_only_message, [$entity_type_id => $entity]); 
        }
        $createMeetingParams->setModeratorOnlyMessage($moderator_only_message_value);
      }
      else {
        // BBB API using TRIM, but trim does not work on unset variables.
        $createMeetingParams->setModeratorOnlyMessage('');
      }
  
      // Set the logo from theme
      if (theme_get_setting('logo.url')) {
        $logoPath = $this->fileUrlGenerator->generateString(theme_get_setting('logo.url'));
        $logoPath = $base_url . '/' . $logoPath;
        if ($logoPath) {
          $createMeetingParams->setLogo($logoPath);
        }
      }

      if (isset($bbb_field->presentation_source) && !empty($bbb_field->presentation_source)) {
        $presentation_source_field_name = $bbb_field->presentation_source;
        if (in_array($presentation_source_field_name, $this->getSupportedPresentationSources($entity))) {          
          $presentations = $entity->{$presentation_source_field_name}->referencedEntities(); 
          foreach ($presentations as $presentation) {
            $file = $this->getFileFromFID($presentation->id());
            if ($file) {
              $file_name = $file->getFilename();
              //$file_data = file_get_contents($file->getFileUri());
              $file_url = \Drupal::service('file_url_generator')->generateAbsoluteString($file->getFileUri());
              \Drupal::logger('debug')->debug('file url: ' . $file_url);
              $createMeetingParams->addPresentation($file_url);
            }
          }
        }
      }

      if (!isset($file_name)) {
        \Drupal::logger('debug')->debug('Default presentation 1');
        if ($fid = $this->getPresentationDefault()) {
          $file = $this->getFileFromFID($fid);
          if ($file) {
            $file_name = $file->getFilename();
            \Drupal::logger('debug')->debug('Default presentation 2');
            //$file_data = file_get_contents($file->getFileUri());
            $file_url = \Drupal::service('file_url_generator')->generateAbsoluteString($file->getFileUri());
            \Drupal::logger('debug')->debug('file url: ' . $file_url);
            $createMeetingParams->addPresentation($file_url);
          }
        }
      } 

      // Define logout url
      if (isset($bbb_field->logout_url) && !empty($bbb_field->logout_url)) {
        $logout_url_value = $bbb_field->logout_url;
        // Replace tokens if token module available
        if ($this->tokenSupport()) { 
          $logout_url_value = \Drupal::token()->replace($logout_url_value, [$entity_type_id => $entity]); 
        }
        $createMeetingParams->setLogoutUrl($logout_url_value);
      }
   
      // Set duration.
      $createMeetingParams->setDuration(0);

      // Set record
      $createMeetingParams->setRecord(TRUE);
    
      // Allow Start Stop Recorindg
      $createMeetingParams->setAllowStartStopRecording(TRUE);


      // Auto record the meeting.
      if ($bbb_field->record) {
        $createMeetingParams->setAutoStartRecording(TRUE);
      }

      // Set a guest policy.     
      if ($bbb_field->guest_policy) {
        $createMeetingParams->setGuestPolicy($bbb_field->guest_policy);
      } 

      // Set mute on start.     
      if ($bbb_field->mute_on_start) {
        $createMeetingParams->setMuteOnStart($bbb_field->mute_on_start);
      } 

      // Create the end meeting callback.
      $meeting_end_callback_url = $this->generateBBBMeetingEndCallback($entity);
      $createMeetingParams->setEndCallbackUrl($meeting_end_callback_url);

      // Set Meta data for BBB Server
      $drupalHost = $this->getHostName();

      $createMeetingParams->addMeta('bbb-origin', 'Drupal');
      if (isset($drupalHost)) {
        $createMeetingParams->addMeta("bbb-origin-server-name", $drupalHost);
      }
      $createMeetingParams->addMeta("bbb-context", $entity->label());
      $createMeetingParams->addMeta("bbb-context-id", $entity->id());
      $createMeetingParams->addMeta("bbb-context-uuid", $entity->uuid());

      try {
        $response = $bbb->createMeeting($createMeetingParams);
        if ($response->getReturnCode() == 'FAILED') {
          $error_message = $this->t("Couldn't create room! please contact system administrator.");
          $this->messenger()->addError($error_message);
        }
        else {
          // Attach our callback url
          //$this->attachBBBHook($entity);
          // Get field machine name
          $bbb_field_name = $bbb_field->getName(); 
          $entity->{$bbb_field_name}->attendee_pw = $response->getAttendeePassword();
          $entity->{$bbb_field_name}->moderator_pw = $response->getModeratorPassword();
          $entity->{$bbb_field_name}->meeting_id = $response->getMeetingId();
          $entity->save();

          $meeting_data = [
            'moderator_pw' => $entity->{$bbb_field_name}->moderator_pw,
            'attendee_pw' => $entity->{$bbb_field_name}->attendee_pw,
            'enabled' => $entity->{$bbb_field_name}->enabled,     
          ];

        }
      }
      catch (\RuntimeException $exception) {
        $this->getLogger('bigbluebutton')->warning($exception->getMessage());
        $error_message = $this->t("Couldn't create room! please contact system administrator.");
        $this->messenger()->addError($error_message);
      }
      catch (\Exception $exception) {
        $this->getLogger('bigbluebutton')->warning($exception->getMessage());
        $error_message = $this->t("Couldn't create room! please contact system administrator.");
        $this->messenger()->addError($error_message);
      }

    }

    return $meeting_data;

  }

  /**
   * Undocumented function
   *
   * @param $url
   *   The url to validate.
   * @return boolean
   *   returns TRUE | FALSE.
   */
  public function isValidExternalURL($url): bool {

    $valid_url = FALSE;

    // If we have a valid url we have to check it realy exists!
    if (UrlHelper::isValid($url, TRUE)) {
      try {
        $client = \Drupal::httpClient();
        $result = $client->request('GET', $url);
        $valid_url = TRUE;
      }
      catch (\Exception $error) {
        $this->getLogger('bigbluebutton')->warning($error->getMessage());
      }
    }

    return $valid_url;

  }

  /**
   * Implements helper for timestamp conversion.
   *
   * @param string $recording_id
   *   The recording id of the recording.
   */
  public function formatRecordingDate(string $recording_id) {

    $recording_date = FALSE;

    if (isset($recording_id)) {
      // The recording_id holds a timestamp
      // after the character -
      // Lets go for that.
      $recording_array = explode("-", $recording_id);
      if (is_array($recording_array)) {
        $recording_js_timestamp = intval($recording_array[1]);
      }
      // Now lets make a proper date.
      if (isset($recording_js_timestamp)) {
        $recording_unix_timestamp = floor($recording_js_timestamp / 1000);
        $recording_date = \Drupal::service('date.formatter')->format($recording_unix_timestamp, 'custom', 'd.m.Y - H:i');
      }
    }

    return $recording_date;

  }

  /**
   * Check for token support
   */

  public function tokenSupport() {

    if ($this->moduleHandler->moduleExists('token')) {
      return TRUE;
    }

    return FALSE;

  }
  
  /**
   * Acts when BBB hits our callback url
   *
   * @param string $meeting_id
   *   The uuid of the entity (meeting id).
   * @param string $meeting_type
   *   The entity type (meeting type).
   * @return void
   */
  public function setActionOnMeetingEndCallback(string $meeting_id, string $meeting_type): void {

    // First we have to check the incoming meeting type
    try {
      $this->entityTypeManager->getDefinition($meeting_type);
    }
    catch (PluginNotFoundException $e) {
      $this->getLogger('bigbluebutton')->warning($e->getMessage());  
      return;
    }

    $entity = \Drupal::service('entity.repository')->loadEntityByUuid($meeting_type, $meeting_id);

    if ($entity) {
      /*
      $bbb_field = $this->getBBBFieldFromEntity($entity);
      // Get field machine name
      $bbb_field_name = $bbb_field->getName();

      $entity->{$bbb_field_name}->meeting_id = $response->getMeetingId();
      $entity->save();
      */
      $this->getLogger('bigbluebutton')->notice('The Meeting End Callback was triggered by BBB Server! Meeting ID: ' . $meeting_id . ' Meeting type: ' . $meeting_type);  
    }

  }

  /**
   * Generates the end meeting callback
   *
   * @param $entity
   *   The entity object.
   * @return string
   *   The callback url as string.
   */
  public function generateBBBMeetingEndCallback($entity): string {

    // Construct callback Url 

    $callback_url = Url::fromRoute('bigbluebutton.meeting_end', [
      'meeting_id' => $entity->uuid(),
      'meeting_type' => $entity->getEntityTypeId()
      ],
      [
        'absolute' => TRUE
      ]  
    );
    
    return urlencode($callback_url->toString());   


  }

  /**
   * Get's the hostname of our backend.
   *
   * @return string
   *   The hostname.
   */
  public function getHostName(): string {
    return \Drupal::request()->getHost();     
  }

  protected function attachBBBHook($entity) {

    $apiUrl = $this->getBBBHost();
    $secretKey = $this->getBBBSecret();

    if (!$this->isValidExternalURL($apiUrl) || empty($secretKey)) {
      return;
    }

    $bbb = new BBB($secretKey, $apiUrl);    

    // Set Callback URL.
    $meeting_end_callback_url = $this->generateBBBMeetingEndCallback($entity);
    
    $HooksCreateParameters = new HooksCreateParameters($meeting_end_callback_url);
    $HooksCreateParameters->setMeetingId($entity->uuid());
    try {
      $hook_response = $bbb->hooksCreate($HooksCreateParameters);
      $return_code = $hook_response->getReturnCode();
      if ($hook_response->getReturnCode() == 'FAILED') {
        $error_message = $this->t("Couldn't create bbb webhook! please contact system administrator.");
        $this->messenger()->addError($error_message);
      }
      else {
        // Message.
        $success_message = $this->t("Successfully registered bbb webhook for meeteing!");
        $this->messenger()->addStatus($success_message);
        // Log It.
        $this->getLogger('bigbluebutton')->info(
          'Successfully attached BBB API callback for node id: @entity_uuid',
          [
            '@entity_uuid' => $entity->uuid(),
          ]
        );
      }
    }
    catch (\RuntimeException $exception) {
      $exception_message = $exception->getMessage();
      $this->getLogger('bigbluebutton')->warning($exception_message);
      $error_message = $this->t("Couldn't create bbb webhook! please contact system administrator.");
      $this->messenger()->addError($error_message);
    }
    catch (Exception $exception) {
      $exception_message = $exception->getMessage();
      $this->getLogger('bigbluebutton')->warning($exception_message);
      $error_message = $this->t("Couldn't create bbb webhook! please contact system administrator.");
      $this->messenger()->addError($error_message);
    }
  }

  /**
   * Get all supported fields for bbb presentation source
   *
   * @param $entity
   *   The given entity.
   * @return array
   *   Supported fields keyed by machine name.
   */
  public function getSupportedPresentationSources($entity) {

    $supported_fields = [];

    $fields = $this->entityFieldManager
      ->getFieldDefinitions($entity->getEntityTypeId(), 
                            $entity->bundle());

    // we can savely loop through our fields. 
    foreach ($fields as $key => $field) {
      if ($field->getType() === 'file') {  
       $field_name = $field->getName();
       $file_extensions = $field->getSetting('file_extensions');
       if ($file_extensions === 'pdf') {
         $supported_fields[$key] = $field_name;
       }
      }
    }    

    return $supported_fields;

  }

  protected function getFileFromFID($fid) {

    $file = $this->entityTypeManager->getStorage('file')->load($fid);
    return $file;

  }

  public function generateJoinMeetingLink($entity = NULL, $entity_uuid = NULL, $entity_type_id = NULL) {

    // Link
    $join_link = NULL;

    // Role
    $role = NULL;

    if (isset($entity_uuid) && isset($entity_type_id)) {
      // Get Entity
      $entity = $this->getEntityFromUUID($entity_uuid, $entity_type_id);
    }
    
    if (!$entity instanceof EntityInterface) {
      \Drupal::logger('bigbluebutton')->warning('Entity not of instance EntityInterface: <pre><code>' . print_r($entity->getEntityTypeId(), TRUE) . '</code></pre>');
      throw new UnprocessableEntityHttpException("The given entity was not found and therefore could not be processed. Please check your entity or UUID and entity type id.");
    }

    $meeting_exists = \Drupal::service('bigbluebutton.helper')->checkMeeting($entity);   

    if (!$meeting_exists) { 
      $meeting_data = \Drupal::service('bigbluebutton.helper')->createMeeting($entity);

      if (isset($meeting_data) && !empty($meeting_data)) {
        $settings['bbb']['moderator_pw'] = $meeting_data['moderator_pw'];
        $settings['bbb']['attendee_pw'] = $meeting_data['attendee_pw'];
        $settings['bbb']['enabled'] = $meeting_data['enabled'];
      } 
    }
    else {
      $meeting_data = $this->getSettingsFromExistingMeeting($entity);
      if (isset($meeting_data)) {
        $settings['bbb']['moderator_pw'] = $meeting_data['moderator_pw'];
        $settings['bbb']['attendee_pw'] = $meeting_data['attendee_pw'];  
        $settings['bbb']['enabled'] = $meeting_data['enabled'];      
      }
    }

    if (isset($meeting_data) && 
        isset($settings) && 
        $settings['bbb']['enabled']
    ) {
      $apiUrl = $this->getBBBHost();
      $secretKey = $this->getBBBSecret();
      if (!$this->isValidExternalURL($apiUrl) || empty($secretKey)) {
        throw new UnprocessableEntityHttpException("The given BBB host and secret are invalid or has become invalid.");
      }
      else {
        $bbb = new BBB($secretKey, $apiUrl);
        if ($entity->access('update', $this->currentUser)) {      
          $role = 'moderator';
          $joinMeetingParams = new JoinMeetingParameters($entity->uuid(), $this->currentUser->getDisplayName(), $settings['bbb']['moderator_pw']);
        }
        elseif ($entity->access('view', $this->currentUser)) {
          $role = 'viewer';
          $joinMeetingParams = new JoinMeetingParameters($entity->uuid(), $this->currentUser->getDisplayName(), $settings['bbb']['attendee_pw']);
        }

        if (isset($joinMeetingParams)) {             
          $joinMeetingParams->setRedirect(TRUE);
          $link = $bbb->getJoinMeetingURL($joinMeetingParams);
          $join_link['link'] = $link;
          $join_link['role'] = $role;
        }
        else {
          throw new UnprocessableEntityHttpException("It seems the user accessing the link has no access to the entity.");
        }
      }
    }
    else {
      $join_link['link'] = 'disabled';
    }  
    
    return $join_link;

  }

  protected function getSettingsFromExistingMeeting($entity) { 

    $meeting_data = NULL;

    $bbb_field = $this->getBBBFieldFromEntity($entity);
    $bbb_field_name = $bbb_field->getName(); 
    $attendee_pw  = $entity->{$bbb_field_name}->attendee_pw; 
    $moderator_pw = $entity->{$bbb_field_name}->moderator_pw;
    $enabled = $entity->{$bbb_field_name}->enabled;

    $meeting_data['enabled'] = $enabled;
    $meeting_data['attendee_pw'] = $attendee_pw;
    $meeting_data['moderator_pw'] = $moderator_pw;

    return $meeting_data;

  }

  protected function getEntityFromUUID($uuid, $entity_type_id) {
    $entities = $this->entityTypeManager->getStorage($entity_type_id)->loadByProperties(['uuid' => $uuid]);
    $entity = reset($entities);
    return $entity;
  }

}


