<?php

namespace Drupal\bigbluebutton;

use Drupal\bigbluebutton\Utility\BigBlueButtonField;

/**
 * Provides property names for Bigbluebutton field values.
 */
class FieldHelper {

  /**
   * Gets the property name matching the given Bigbluebutton field value.
   *
   * @param string $field
   *   An Bigbluebuttion field.
   *
   * @return string
   *   The property name.
   */
  public static function getPropertyName($field) {
    $property_mapping = [
      BigBlueButtonField::ENABLED => 'enabled',
      BigBlueButtonField::WELCOME => 'welcome',
      BigBlueButtonField::LOGOUT_URL => 'logout_url',
      BigBlueButtonField::MODERATOR_ONLY_MESSAGE => 'moderator_only_message',
      BigBlueButtonField::GUEST_POLICY => 'guest_policy',
      BigBlueButtonField::RECORD => 'record',
      BigBlueButtonField::MUTE_ON_START => 'mute_on_start',
      BigBlueButtonField::PRESENTATION_SOURCE => 'presentation_source',
    ];

    return $property_mapping[$field] ?? NULL;
  }
  
}