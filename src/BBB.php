<?php

namespace Drupal\bigbluebutton;

use BigBlueButton\BigBlueButton;
use BigBlueButton\Util\UrlBuilder;

/**
 * Defines Virtual Event BBB.
 */
class BBB extends BigBlueButton {

  /**
   * Constructs a new BigBlueButton object.
   */
  public function __construct($securitySecret, $bbbServerBaseUrl) {
    $this->securitySecret   = $securitySecret;
    $this->bbbServerBaseUrl = $bbbServerBaseUrl;
    $this->urlBuilder       = new UrlBuilder($this->securitySecret, $this->bbbServerBaseUrl);
  }

}
