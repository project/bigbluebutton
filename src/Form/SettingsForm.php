<?php declare(strict_types = 1);

namespace Drupal\bigbluebutton\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Component\Utility\Environment;
use Drupal\file\Entity\File;

/**
 * Configure Bigbluebutton settings for this site.
 */
final class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'bigbluebutton_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['bigbluebutton.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['hostname'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Hostname'),
      '#default_value' => $this->config('bigbluebutton.settings')->get('hostname'),
      '#required' => TRUE
    ];
    $form['secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret'),
      '#default_value' => $this->config('bigbluebutton.settings')->get('secret'),
      '#required' => TRUE
    ];     

    $max_size = Environment::getUploadMaxSize();
    if (isset($max_size) && is_numeric($max_size)) {
      $max_size_mb = $max_size / 1024 / 1024;
    }
    else {
      $max_size_mb = 0;
    }

    $form['presentation_default'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Presentation default'),
      '#upload_location' => 'public://',
      '#upload_validators' => [
        'file_validate_extensions' => ['pdf'],
        'file_validate_size' => [$max_size],
      ],
      '#description' => $this->t('You can upload up to @max_size MB', ['@max_size' => $max_size_mb]),
    ];

    if ($fid = $this->config('bigbluebutton.settings')->get('presentation_default')) {
      $form['presentation_default']['#default_value'] = [$fid];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {   
    // If we have a valid url we have to check it realy exists!
    if (!\Drupal::service('bigbluebutton.helper')->isValidExternalURL($form_state->getValue('hostname'))) {
      $form_state->setErrorByName('hostname', $this->t('The entered URL is not valid!'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {    

    $fid = reset($form_state->getValue('presentation_default'));

    if ($fid) {
      $file = File::load($fid);
      $file->setPermanent();
      $file->save();
    }
    else {
      $fid = '';
    }

    $this->config('bigbluebutton.settings')
      ->set('hostname', $form_state->getValue('hostname'))
      ->set('secret', $form_state->getValue('secret'))
      ->set('presentation_default', $fid)
      ->save();
    parent::submitForm($form, $form_state);
  }

}
