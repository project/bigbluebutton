<?php

namespace Drupal\bigbluebutton\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\bigbluebutton\BBB;
use BigBlueButton\Parameters\DeleteRecordingsParameters;
use Drupal\Core\Url;
use Drupal\Core\Logger\LoggerChannelTrait;

/**
 *
 */
class BBBDeleteRecordingForm extends FormBase {

  use LoggerChannelTrait;

  /**
   * @var array settings
   */
  protected $settings;  

  /**
   * @param array settings
   */
  public function __construct(array $settings = null) {
    $this->settings = $settings;
  }  

  /**
   * Get the recording ID of a recording and sends API Call to delete it.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {

    $form_id = 'bbb_delete_recording';

    static $count = 0;
    $count++;

    return $form_id . '_' . $count;

  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $recording_id = NULL, $settings = NULL) {

    $this->settings = $settings; 

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['recording_id'] = [
      '#type' => 'hidden',
      '#value' => $recording_id,
    ];

    $form['actions']['delete'] = [
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#attributes' => ['onclick' => 'if(!confirm("Are you sure you want to delete that recording?")){return false;}'],
    ];

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $recording_id = $form_state->getValue('recording_id');

    if (isset($recording_id)) {

      $apiUrl = \Drupal::config('bigbluebutton.settings')->get('hostname');
      $secretKey = \Drupal::config('bigbluebutton.settings')->get('secret');
      $bbb = new BBB($secretKey, $apiUrl);

      $deleteRecordingsParameters = new DeleteRecordingsParameters($recording_id, $this->settings['bbb']['moderator_pw']);

        try {
          $response = $bbb->deleteRecordings($deleteRecordingsParameters);
        }
        catch (\RuntimeException $exception) {
          $this->getLogger('bigbluebutton')->warning($exception->getMessage());
          $error_message = $this->t("Couldn't get meeting info! please contact system administrator.");
          $this->messenger()->addError($error_message);
        }
      }
    }
  

}