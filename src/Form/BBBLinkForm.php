<?php

namespace Drupal\bigbluebutton\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use BigBlueButton\Parameters\JoinMeetingParameters;
use Drupal\bigbluebutton\BBB;

/**
 * Defines Virtual Event BBB Link Form.
 */
class BBBLinkForm extends FormBase {

  /**
   * @var array settings
   */
  protected $settings;

  /**
   * @param array settings
   */
  public function __construct(array $settings = null) {
    $this->settings = $settings;
  }  


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bbb_link_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $settings = null) {

    $this->settings = $settings; 

    // Check the current user
    $user = \Drupal::currentUser();

    // We want to display our button when 
    // BBB enabled only!

    if ($this->settings['bbb']['enabled']) {
      $form['submit'] = [
        '#type' => 'submit',
        '#value' => $this->settings['link_title'],
        '#attributes' => ['class' => [$this->settings['link_classes']]],
      ];
    }

    $form['#attributes'] = ['target' => '_blank'];

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValues() as $key => $value) {
      // @todo Validate fields.
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {


    try {
      $join_link = \Drupal::service('bigbluebutton.helper')->generateJoinMeetingLink($this->settings['entity']);
    }
    catch (\RuntimeException $exception) {
      $this->getLogger('bigbluebutton')->warning($exception->getMessage());
      $error_message = $this->t("Couldn't get meeting join link! please contact system administrator.");
      $this->messenger()->addError($error_message);
    }
    catch (\Exception $exception) {
      $this->getLogger('bigbluebutton')->warning($exception->getMessage());
      $error_message = $this->t("Couldn't get meeting join link! please contact system administrator.");
      $this->messenger()->addError($error_message);
    }  

    if (isset($join_link) && $join_link['link'] !== 'disabled') {
      $url = $join_link['link'];
      $form_state->setResponse(new TrustedRedirectResponse($url));
    }  



  }
}
