<?php

namespace Drupal\bigbluebutton\Utility;

use Drupal\bigbluebutton\Utility\AbstractEnum;

/**
 * Enumerates available bigbluebutton fields.
 *
 * @codeCoverageIgnore
 */
final class BigBlueButtonField extends AbstractEnum {

    public const ENABLED = 'enabled';
    public const WELCOME = 'welcome';
    public const LOGOUT_URL = 'logout_url';
    public const GUEST_POLICY = 'guest_policy';
    public const MODERATOR_ONLY_MESSAGE = 'moderator_only_message';
    public const MUTE_ON_START = 'mute_on_start';
    public const RECORD = 'record';
    public const PRESENTATION_SOURCE = 'presentation_source';
    public const MODERATOR_PW = 'moderator_pw';
    public const ATTENDEE_PW = 'attendee_pw';
    public const MEETING_ID = 'meeting_id';

}