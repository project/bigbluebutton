<?php

namespace Drupal\bigbluebutton\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;
use Psr\Log\LoggerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use BigBlueButton\Parameters\JoinMeetingParameters;
use Drupal\bigbluebutton\BigBlueButtonHelper;
use Drupal\rest\ModifiedResourceResponse;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * Provides a resource to get meeting info.
 * @RestResource(
 *   id = "bigbluebutton_meeting_info_rest_resource",
 *   label = @Translation("Get Meeeting Info Rest Resource"),
 *   uri_paths = {
 *     "canonical" = "/api/bigbluebutton/meeting-info/{entity_type_id}/{entity_uuid}"
 *   }
 * )
 */
class BigBlueButtonGetMeetingInfo extends ResourceBase {
  /**
   * A current user instance which is logged in the session.
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

 /**
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * 
   * @var \Drupal\bigbluebutton\BigBlueButtonHelper
   */
  protected $helper;

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $config
   *   A configuration array which contains the information about the plugin instance.
   * @param string $module_id
   *   The module_id for the plugin instance.
   * @param mixed $module_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A currently logged user instance.
   */
  public function __construct(
    array $config,
    $module_id,
    $module_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user,
    Request $current_request,
    BigBlueButtonHelper $helper) {
    parent::__construct($config, $module_id, $module_definition, $serializer_formats, $logger);
    $this->currentUser = $current_user;
    $this->currentRequest = $current_request;
    $this->helper = $helper;
  }
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $config, $module_id, $module_definition) {
    return new static(
      $config,
      $module_id,
      $module_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('bigbluebutton'),
      $container->get('current_user'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('bigbluebutton.helper'),
    );
  }
  /**
   * Responds to GET request.
   * Returns a list of taxonomy terms.
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   * Throws exception expected.
   */
  public function get(string $entity_type_id, string $entity_uuid) {

    $data = [];

    // Join link
    $join_link = NULL;
    // Current user
    $user = $this->currentUser;  
    // Default error message
    $error_message = 'Not possible to generate the join link! Please check your API Settings in the backend.';  
  
    try {
      $data = \Drupal::service('bigbluebutton.helper')->getMeetingInfo($entity_type_id, $entity_uuid);      
      $response = new ModifiedResourceResponse($data, 200); 

    }
    catch (\RuntimeException $exception) {
      $this->logger->warning($exception->getMessage());
      $error_message = $exception->getMessage();
      $response = new ModifiedResourceResponse(['error' => $error_message], 400);
    } 
    catch (UnprocessableEntityHttpException $exception) {
      $this->logger->warning($exception->getMessage());
      $error_message = "Couldn't get meeting info! please contact system administrator.";
      $response = new ModifiedResourceResponse(['error' => $error_message], 422);      
    }
    catch (\Exception $exception) {
      $this->logger->warning($exception->getMessage());
      $error_message = "Couldn't get meeting info! please contact system administrator.";
      $response = new ModifiedResourceResponse(['error' => $error_message], 400);
    } 

    return $response;      

  }

}
