<?php declare(strict_types = 1);

namespace Drupal\bigbluebutton\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use BigBlueButton\Parameters\GetRecordingsParameters;
use Drupal\bigbluebutton\BBB;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'Big Blue Button Recordings' formatter.
 *
 * @FieldFormatter(
 *   id = "bigbluebutton_recordings",
 *   label = @Translation("BBB Recordings"),
 *   field_types = {"bigbluebutton"},
 * )
 */
final class BigBlueButtonRecordingsFormatter extends FormatterBase {

  use MessengerTrait;
  use LoggerChannelTrait;

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    $setting = [
      'display_options' => 'links',
    ];
    return $setting + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $elements['display_options'] = [
      '#type' => 'select',
      '#title' => $this->t('Display options'),
      '#options' => ['thumbnail' => 'thumbnail', 'video' => 'video', 'links' => 'links'],
      '#default_value' => $this->getSetting('display_options'),
    ];
    return $elements;
  }  

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $element = [];

    $item = $items[0];

    $display_options = $this->getSetting('display_options');

    $entity = $items->getEntity();

    $settings = [
      'display_options' => $this->getSetting('display_options'),
      'entity' => $items->getEntity(),
      'bbb' => [
        'welcome' => $item->welcome,
        'logout_url' => $item->logout_url,
        'guest_policy' => $item->guest_policy,
        'moderator_pw' => $item->moderator_pw,
        'attendee_pw' => $item->attendee_pw,
        'mute_on_start' => $item->mute_on_start,
        'record' => $item->record,
        'meeting_id' => $item->meeting_id,
        'enabled' => $item->enabled        
      ]
    ];

    $apiUrl = \Drupal::config('bigbluebutton.settings')->get('hostname');
    $secretKey = \Drupal::config('bigbluebutton.settings')->get('secret');
    $bbb = new BBB($secretKey, $apiUrl);

    $recordingParams = new GetRecordingsParameters();
    $recordingParams->setMeetingID($entity->uuid());


    try {
      $response = $bbb->getRecordings($recordingParams);
      if (!empty($response->getRawXml()->recordings->recording)) {
        $i = 1;
        $recordings = [];
        foreach ($response->getRawXml()->recordings->recording as $key => $recording) {
          foreach ($recording->playback as $key => $playback) {
            foreach ($recording->playback->format as $key => $format) {
              if ($format->type == "video_mp4" || $format->type == "video" || $format->type == "presentation" || $format->type == 'screenshare') {
                $format->recordID = $recording->recordID;
                $recording_id = $recording->recordID->__toString();
                $recording_date = \Drupal::service('bigbluebutton.helper')->formatRecordingDate($recording_id);
                $delete_form = FALSE;
                if ($entity->access('update')) {
                  $delete_form = \Drupal::formBuilder()->getForm('\Drupal\bigbluebutton\Form\BBBDeleteRecordingForm', $recording_id, $settings);
                }
                $recordings[] = [
                  'recording_id' => $recording_id,
                  'recording' => $format,
                  'recording_position' => $i,
                  'recording_link' => $recording->url->__toString(),
                  'recording_date' => $recording_date,
                  'delete_form' => $delete_form,
                ];
                $i = $i + 1;
              }
            }
          }
        }



        switch ($display_options) {
          case 'links':
            $element[0] = [
              '#theme' => 'bbb_recordings_links',
              '#url' => Url::fromRoute('bigbluebutton.view_recording', ['entity_uuid' => $entity->uuid(), 'recording_id' => $recordings['recording_id']]),
              '#display_options' => $display_options,
              '#recordings' => $recordings,
            ];
            break;

          case 'thumnails':
            $element[0] = [
              '#theme' => 'bbb_recordings_thumbnails',
              '#url' => Url::fromRoute('bigbluebutton.view_recording', ['entity_uuid' => $entity->uuid()]),
              '#display_options' => $display_options,
              '#recordings' => $recordings,
            ];
            break;

          case 'video':
            $element[0] = [
              '#theme' => 'bbb_recordings_video',
              '#recordings' => $recordings,
            ];
            break;

          default:
            $element[0] = [
              '#theme' => 'bbb_recordings_links',
              '#url' => Url::fromRoute('bigbluebutton.view_recording', ['entity_uuid' => $entity->uuid()]),
              '#display_options' => $display_options,
              '#recordings' => $recordings,
            ];
            break;
        }
      }
    }
    catch (\RuntimeException $exception) {
      $this->getLogger('bigbluebutton')->warning($exception->getMessage());
      $error_message = $this->t("Couldn't get recordings! please contact system administrator.");
      $this->messenger()->addError($error_message);
    }

    //dump($element);
    //exit;

    return $element;

  }

}
