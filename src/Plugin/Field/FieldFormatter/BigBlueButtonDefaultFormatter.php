<?php declare(strict_types = 1);

namespace Drupal\bigbluebutton\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\bigbluebutton\BigBlueButtonHelper;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * Plugin implementation of the 'BBB Field' formatter.
 *
 * @FieldFormatter(
 *   id = "bigbluebutton_default",
 *   label = @Translation("BBB Default"),
 *   field_types = {"bigbluebutton"},
 * )
 */
final class BigBlueButtonDefaultFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    $setting = [
      'link_title' => 'Join meeting',
      'link_classes' => '',
    ];
    return $setting + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $elements['link_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Link title'),
      '#default_value' => $this->getSetting('link_title'),
    ];
    $elements['link_classes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Link classes'),
      '#default_value' => $this->getSetting('link_classes'),
    ];
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    return [
      $this->t('Link title: @link_title', ['@link_title' => $this->getSetting('link_title')]),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {

    $elements = [];

    $entity = $items->getEntity();

    $settings = [
      'link_title' => $this->getSetting('link_title'),
      'link_classes' => $this->getSetting('link_classes'),
      'entity' => $entity
    ]; 

    foreach ($items as $delta => $item) {
      $settings['bbb'] = [
        'enabled' => $item->enabled,
      ];  
      
      $elements[$delta] = \Drupal::formBuilder()->getForm('Drupal\bigbluebutton\Form\BBBLinkForm', $settings);
    }
    
    return $elements;

  }
}
