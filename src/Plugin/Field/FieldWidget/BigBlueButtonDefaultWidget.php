<?php declare(strict_types = 1);

namespace Drupal\bigbluebutton\Plugin\Field\FieldWidget;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\bigbluebutton\Utility\BigBlueButtonField;

/**
 * Defines the 'bigbluebutton_default' field widget.
 *
 * @FieldWidget(
 *   id = "bigbluebutton_default",
 *   label = @Translation("Big Blue Button"),
 *   field_types = {
 *     "bigbluebutton"
 *   },
 * )
 */
final class BigBlueButtonDefaultWidget extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;  

  /**
   * Constructs a AddressDefaultWidget object.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, ConfigFactoryInterface $config_factory) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new self(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $entity = $items[$delta]->getEntity();
    $moderator_only_message_default = '';        
    $item = $items[$delta];
    $value = $item->toArray();
    $welcome = $items[$delta]->welcome ?? '';
    $logout_url = $items[$delta]->logout_url ?? '';

    if ($items[$delta]->guest_policy == 0) {
      $guest_policy = 'ALWAYS_ACCEPT';
    }

    $guest_policy = $items[$delta]->guest_policy ?? 'ALWAYS_ACCEPT';

    $moderator_only_message = $items[$delta]->moderator_only_message ?? $moderator_only_message_default;
    $record = $items[$delta]->record ?? '';
    $mute_on_start = $items[$delta]->mute_on_start ?? '';
    $enabled = $items[$delta]->enabled ?? FALSE;
    $presenation_source = $items[$delta]->presentation_source ?? '';
    $presenation_source_options_available = FALSE;
    $presenation_source_options = \Drupal::service('bigbluebutton.helper')->getSupportedPresentationSources($entity);  
    if (isset($presenation_source_options) && !empty($presenation_source_options)) {
      $presenation_source_options_available = TRUE;
    }

    $field_name = $items[$delta]->getFieldDefinition()->getName();
    $input_name = $field_name . "[0][enabled]";

    $meeting_id = $items[$delta]->meeting_id ?? '';
    
    $moderator_pw = $items[$delta]->moderator_pw ?? '';

    $attendee_pw = $items[$delta]->attendee_pw ?? '';

    $element += [
      '#type' => 'details',
      '#open' => TRUE,
      '#description' => 'BBB Field'
    ];
    
    $element['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable BBB Meeting'),
      '#default_value' => $enabled,
      '#description' => $this->t('Tick to enable BBB Meeting functionality.'),
    ];    

    $element['welcome'] = [
      '#title' => $this->t('Welcome message'),
      '#type' => 'textfield',
      '#default_value' => $welcome,
      '#size' => 255,
      '#maxlength' => 255,
      //'#element_validate' => [
        //[$this, 'validateWelcomeMessage'],
      //],
      '#description' => $this->t('Here you can set a welcome message for all attendees. Please note you can make use of drupal tokens in this field (Ex: node:url). The tokens will be replaced once the meeting has been created.'),
      '#states' => [
        'disabled' => [
          ":input[name='$input_name']" => ['checked' => FALSE],
        ]
      ]
    ];

    $element['logout_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Logout url'),
      '#default_value' => $logout_url,
      '#size' => 255,
      '#maxlength' => 255,
      //'#element_validate' => [
        //[$this, 'validateLogoutUrl'],
      //],
      '#description' => $this->t('Here you can set a custom url where the user will be redirected once the meeting has finished.'),
      '#states' => [
        'disabled' => [
          ":input[name='$input_name']" => ['checked' => FALSE],
        ]
      ]
    ];

    $element['guest_policy'] = [
      '#type' => 'select',
      '#title' => $this->t('Guest policy'),
      '#options' => ['ALWAYS_ACCEPT' => 'ALWAYS_ACCEPT',
                     'ALWAYS_DENY' => 'ALWAYS_DENY',
                     'ASK_MODERATOR' => 'ASK_MODERATOR'],
      '#default_value' => $guest_policy,
      '#maxlength' => 50,
      //'#element_validate' => [
        //[$this, 'validateLogoutUrl'],        
      //],
      '#description' => $this->t('Will set the guest policy for the meeting. The guest policy determines whether or not users who send a join request with guest=true will be allowed to join the meeting. Possible values are ALWAYS_ACCEPT, ALWAYS_DENY, and ASK_MODERATOR.'),
      '#states' => [
        'disabled' => [
          ":input[name='$input_name']" => ['checked' => FALSE],
        ]
      ]      
    ];

    $element['moderator_only_message'] = [
      '#title' => $this->t('Moderator message'),
      '#type' => 'textfield',
      '#default_value' => $moderator_only_message,
      '#size' => 255,
      '#maxlength' => 255,
      //'#element_validate' => [
        //[$this, 'validateWelcomeMessage'],
      //],
      '#description' => $this->t('Define a message for moderators. Please note you can make use of drupal tokens in this field (Ex: node:url). The tokens will be replaced once the meeting has been created.'),
      '#states' => [
        'disabled' => [
          ":input[name='$input_name']" => ['checked' => FALSE],
        ]
      ]
    ];

    $element['record'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Auto record'),
      '#default_value' => $record,
      '#description' => $this->t('Setting record to true instructs the BigBlueButton server to auto-record the media and events in the session for later playback.'),
      '#states' => [
        'disabled' => [
          ":input[name='$input_name']" => ['checked' => FALSE],
        ]
      ]
    ];

    $element['mute_on_start'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Mute on start'),
      '#default_value' => $mute_on_start,
      '#description' => $this->t('will mute all users when the meeting starts.'),
      '#states' => [
        'disabled' => [
          ":input[name='$input_name']" => ['checked' => FALSE],
        ]
      ],
    ];

    

    $element['moderator_pw'] = [
      '#title' => $this->t('Moderator pw'),
      '#type' => 'hidden',
      '#default_value' => $moderator_pw,
      '#size' => 255,
      '#maxlength' => 255,
      //'#element_validate' => [
        //[$this, 'validateWelcomeMessage'],
      //],
    ];

  

    $element['meeting_id'] = [
      '#title' => $this->t('Meeting ID'),
      '#type' => 'hidden',
      '#default_value' => $meeting_id,
      '#size' => 255,
      '#maxlength' => 255,
      //'#element_validate' => [
        //[$this, 'validateWelcomeMessage'],
      //],
    ];

    $element['attendee_pw'] = [
      '#title' => $this->t('Attendee pw'),
      '#type' => 'hidden',
      '#default_value' => $attendee_pw,
      '#size' => 255,
      '#maxlength' => 255,
      //'#element_validate' => [
        //[$this, 'validateWelcomeMessage'],
      //],
    ];

    

    $element['presentation_source'] = [
      '#type' => 'select',
      '#title' => $this->t('Presentation source'),
      '#description' => $this->t('Select available fields for big blue button presentations.'),
      '#options' => $presenation_source_options, 
      '#default_value' => $presenation_source,
      '#access' => $presenation_source_options_available,     
      '#states' => [
        'disabled' => [
          ":input[name='$input_name']" => ['checked' => FALSE],
        ]
      ],
    ];

    $field_overrides = $items[$delta]->getFieldOverrides();

    foreach ($field_overrides as $key => $type) {
      if ($type === 'hidden') {
        $element[$key]['#access'] = FALSE;
      }
      elseif ($type === 'required') {
        $element[$key]['#required'] = TRUE;
      }
    }
  
    return $element;

  }

    /**
   * Validate the welcome message text field.
   */
  public function validateWelcomeMessage($element, FormStateInterface $form_state) {
    $value = $element['#value'];
   
    
    if (strlen($value) === 0) {
      $form_state->setValueForElement($element, '');
      return;
    }

  }

  /**
   * Validate the logout url field.
   */
  public function validateLogoutUrl($element, FormStateInterface $form_state) {
    $value = $element['#value'];
   
    
    if (strlen($value) === 0) {
      $form_state->setValueForElement($element, '');
      return;
    }

  }

  /**
   * Form API callback: Makes all address field properties optional.
   */
  public static function makeFieldsOptional(array $element, FormStateInterface $form_state) {
    foreach (Element::getVisibleChildren($element) as $key) {
      if (!empty($element[$key]['#required'])) {
        $element[$key]['#required'] = FALSE;
      }
    }
    return $element;
  } 

  /**
   * Reconstruct the values because of our multiple 
   * field properties.
   */  
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {

    // Get all field properties
    $field_properties = [];

    $new_values = [];
    foreach ($values as $delta => $value) {
      if (is_array($value)) {
        foreach ($value as $key => $property_value) {
          if (BigBlueButtonField::exists($key)) {
            $field_properties[$key] = $property_value;
          }
        }
      }        
      $new_values[$delta] = $field_properties;     
    }

    return $new_values;

  } 

}
