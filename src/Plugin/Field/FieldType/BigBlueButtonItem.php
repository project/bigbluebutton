<?php declare(strict_types = 1);

namespace Drupal\bigbluebutton\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\bigbluebutton\Utility\FieldOverride;
use Drupal\bigbluebutton\Utility\FieldOverrides;
use Drupal\Core\Form\FormStateInterface;
use Drupal\bigbluebutton\FieldHelper;

/**
 * Defines the 'bigbluebutton' field type.
 *
 * @FieldType(
 *   id = "bigbluebutton",
 *   label = @Translation("Big Blue Button"),
 *   description = @Translation("An entity field containing a Big blue button integration."),
 *   category = @Translation("BBB"),
 *   default_widget = "bigbluebutton_default",
 *   default_formatter = "bigbluebutton_default",
 *   cardinality = 1
 * )
 */
final class BigBlueButtonItem extends FieldItemBase {


  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition): array {

    // @DCG
    // See /core/lib/Drupal/Core/TypedData/Plugin/DataType directory for
    // available data types.
    $properties['welcome'] = DataDefinition::create('string')
      ->setLabel(t('Welcome message'));
    $properties['logout_url'] = DataDefinition::create('string')
      ->setLabel(t('Logout url'));
    $properties['guest_policy'] = DataDefinition::create('string')
      ->setLabel(t('Guest policy'));
    $properties['moderator_only_message'] = DataDefinition::create('string')
      ->setLabel(t('Moderator message'));
    $properties['record'] = DataDefinition::create('boolean')
      ->setLabel(t('Record meeting'));
    $properties['mute_on_start'] = DataDefinition::create('boolean')
      ->setLabel(t('Mute on start'));
    $properties['moderator_pw'] = DataDefinition::create('string')
      ->setLabel(t('Moderator Password'))
      ->setInternal(TRUE);
    $properties['attendee_pw'] = DataDefinition::create('string')
      ->setLabel(t('Attendee Password'))
      ->setInternal(TRUE);
    $properties['meeting_id'] = DataDefinition::create('string')
      ->setLabel(t('Meeting ID'));
    $properties['enabled'] = DataDefinition::create('boolean')
      ->setLabel(t('BBB Enabled'));
    $properties['presentation_source'] = DataDefinition::create('string')
      ->setLabel(t('Presentation source'));

    return $properties;

  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition): array {

    $columns = [
      'welcome' => [
        'type' => 'varchar',
        'not null' => FALSE,
        'description' => 'A welcome message that gets displayed on the chat window when the participant joins. You can include keywords (%%CONFNAME%%, %%DIALNUM%%, %%CONFNUM%%) which will be substituted automatically.',
        'length' => 255,
      ],
      'logout_url' => [
        'type' => 'varchar',
        'not null' => FALSE,
        'description' => 'The URL that the users will be redirected to after they logs out of the conference, leave empty to redirect to the current entity.',
        'length' => 255,
      ],
      'guest_policy' => [
        'type' => 'varchar',
        'not null' => TRUE,
        'description' => 'The guest policy determines whether or not users who send a join request with guest=true will be allowed to join the meeting.',
        'length' => 255,
      ],
      'moderator_only_message' => [
        'type' => 'varchar',
        'not null' => FALSE,
        'description' => 'A message that gets displayed on the chat window when the moderator joins.',
        'length' => 255,
      ],
      'record' => [
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'description' => 'Whether to automatically start recording when first user joins, Moderators in the session can still pause and restart recording using the UI control.',
      ],
      'mute_on_start' => [
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'description' => 'Setting true will mute all users when the meeting starts.',
      ],
      'moderator_pw' => [
        'type' => 'varchar',
        'not null' => FALSE,
        'description' => 'Password for the moderator.',
        'length' => 255,
      ],
      'attendee_pw' => [
        'type' => 'varchar',
        'not null' => FALSE,
        'description' => 'Password for the assignee.',
        'length' => 255,
      ],
      'meeting_id' => [
        'type' => 'varchar',
        'not null' => FALSE,
        'description' => 'Meeting ID.',
        'length' => 255,
      ],
      'enabled' => [
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'description' => 'Whether the BBB Meeting is enabeld.',
      ],
      'presentation_source' => [
        'type' => 'varchar',
        'not null' => FALSE,
        'description' => 'Presentation source.',
        'length' => 255,
      ]      
    ];

    $schema = [
      'columns' => $columns,
      // @todo Add indexes here if necessary.
    ];

    return $schema;
  }

  /**
   * Gets the field overrides for the current field.
   *
   * @return array
   *   FieldOverride constants keyed by AddressField constants.
   */
  public function getFieldOverrides() {
    $field_overrides = [];
    if ($fields = $this->getSetting('fields')) {
      $unused_fields = array_diff($this->getAllFields(), $fields);
      foreach ($unused_fields as $field) {
        $field_overrides[$field] = FieldOverride::HIDDEN;
      }
    }
    elseif ($overrides = $this->getSetting('field_overrides')) {
      foreach ($overrides as $field => $data) {
        $field_overrides[$field] = $data['override'];
      }
    }

    return $field_overrides;
  }
  
  public function getAllFields() {

    $fields = [
      'welcome' => ['label' => $this->t('Welcome message'), 'value' => null],
      'logout_url' => ['label' => $this->t('Logout url'), 'value' => null],
      'guest_policy' => ['label' => $this->t('Guest policy'), 'value' => null],
      'moderator_only_message' => ['label' => $this->t('Moderator messgae'), 'value' => null],
      'record' => ['label' => $this->t('Record'), 'value' => null],
      'mute_on_start' => ['label' => $this->t('Mute on start'), 'value' => null],
      'presentation_source' => ['label' => $this->t('Presentation_source'), 'value' => null],      
    ];

    return $fields;

  }

  /**
   * {@inheritdoc}
   */
  public function getProperties($include_computed = FALSE) {
    $properties = parent::getProperties($include_computed);
    $parsed_overrides = new FieldOverrides($this->getFieldOverrides());
    $hidden_properties = array_map(static function ($name) {
      return FieldHelper::getPropertyName($name);
    }, $parsed_overrides->getHiddenFields());
    foreach ($hidden_properties as $hidden_property) {
      unset($properties[$hidden_property]);
    }
    return $properties;
  }  



  /**
   * {@inheritdoc}
   */
  public function getWelcome(): string {
    return $this->welcome ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function getLogoutUrl(): string {
    return $this->logout_url ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function getGuestPolicy(): string {
    return $this->guest_policy ?? 'ALWAYS_ACCEPT';
  }

  /**
   * {@inheritdoc}
   */
  public function getModeratorOnlyMessage(): string {
    return $this->moderator_only_message ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function getRecord(): bool {
    return $this->record ?? FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getEnabled(): bool {
    return $this->enabled ?? FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getPresentationSource(): string {
    return $this->presentation_source ?? '';
  }  

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'field_overrides' => [],
      // Replaced by field_overrides.
      'fields' => [],
    ] + parent::defaultFieldSettings();

  }  

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {


    $element['field_overrides_title'] = [
      '#type' => 'item',
      '#title' => $this->t('Field overrides'),
      '#description' => $this->t('Use field overrides to override the Big Blue Button settings, forcing specific properties to always be hidden, optional, or required.'),
    ];
    $element['field_overrides'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Property'),
        $this->t('Override'),
      ],
      '#element_validate' => [[get_class($this), 'fieldOverridesValidate']],
    ];
    $field_overrides = $this->getFieldOverrides();

    
    foreach ($this->getGenericFieldLabels() as $field_name => $label) {
      $override = $field_overrides[$field_name] ?? '';

      $element['field_overrides'][$field_name] = [
        'field_label' => [
          '#type' => 'markup',
          '#markup' => $label,
        ],
        'override' => [
          '#type' => 'select',
          '#options' => [
            FieldOverride::HIDDEN => $this->t('Hidden'),
            FieldOverride::OPTIONAL => $this->t('Optional'),
            FieldOverride::REQUIRED => $this->t('Required'),
          ],
          '#default_value' => $override,
          '#empty_option' => $this->t('- No override -'),
        ],
      ];
    }

    return $element;
  }

  protected function getGenericFieldLabels() {

    return [
      'welcome' => t('Welcome message', [], ['context' => 'BBB label']),
      'logout_url' => t('Logout URL', [], ['context' => 'BBB label']),
      'guest_policy' => t('Guest policy', [], ['context' => 'BBB label']),
      'moderator_only_message' => t('Moderator message', [], ['context' => 'BBB label']),
      'record' => t('Auto Record', [], ['context' => 'BBB label']),
      'mute_on_start' => t('Mute on start', [], ['context' => 'BBB label']),
      'presentation_source' => t('Presentation source', [], ['context' => 'BBB label']),      
    ];


  }

  /**
   * Form element validation handler: Removes empty field overrides.
   *
   * @param array $element
   *   The field overrides form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the entire form.
   */
  public static function fieldOverridesValidate(array $element, FormStateInterface $form_state) {
    $overrides = $form_state->getValue($element['#parents']);
    $overrides = array_filter($overrides, function ($data) {
      return !empty($data['override']);
    });
    $form_state->setValue($element['#parents'], $overrides);
  }
 



}


