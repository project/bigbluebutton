## INTRODUCTION

Big Blue Button Integration as a field.

## REQUIREMENTS

There is no dependency for this module. The BBB PHP API will be installed automatically for you.

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION
- Visit admin/config/system/bbb-settings and enter your BBB Host and Secret.
- You could also add a default presentation as pdf.

## MAINTAINERS

Current maintainers for Drupal 10:

- PAUL MRVIK (globexplorer) - https://www.drupal.org/u/globexplorer

## REST 

As this Big Blue Button integration comes as a field you can simply use jsonapi for setting the meeting up. You will get a response that includes the meeting id which is basically the uuid of the entity. To get the join meeting link for the current user, you have to enable a custom rest endpoint, which delivers the join meeting link.

- Enable 'Get Join Meeting Link Get Rest Resource' in restui
- Enable Permission 'Access GET on Get Join Meeting Link Get Rest Resource resource'

Now you are ready to call the endpoint to get the join meeting link. See here an example request:

Parameters:
(string) entity_type_id
(string) entity_uuid

```
https://{YOUR_DOMAIN}/api/bigbluebutton/join-meeting-link/{entity_type_id}/{entity_uuid}?_format=json

```

Replace node with any content entity in your system and also submit the uuid of the entity.

Get Meeting Info:

Parameters:
(string) entity_type_id
(string) entity_uuid

```
https://{YOUR_DOMAIN}/api/bigbluebutton/meeting-info/node/3bb08b23-2a3a-4ddd-9d0d-8e27be3e2651?_format=json

```
Example Response:

```

{
    "bbb": {
        "returncode": "SUCCESS",
        "meetingName": "test",
        "meetingID": "xb59ca74-65d5-4a08-8a6d-2d21f2aff154",
        "internalMeetingID": "941ff459fe45335f84e3a0182c08cdc481c5f846-1730789281319",
        "createTime": "1730789281319",
        "createDate": "Tue Nov 05 07:48:01 CET 2024",
        "voiceBridge": "49727",
        "dialNumber": "613-555-1234",
        "attendeePW": "W9o8aw0v",
        "moderatorPW": "pYvWdyjr",
        "running": "true",
        "duration": "0",
        "hasUserJoined": "true",
        "recording": "true",
        "hasBeenForciblyEnded": "false",
        "startTime": "1730789281338",
        "endTime": "0",
        "participantCount": "1",
        "listenerCount": "1",
        "voiceParticipantCount": "0",
        "videoCount": "0",
        "maxUsers": "0",
        "moderatorCount": "1",
        "attendees": {
            "attendee": {
                "userID": "w_khizxf9xos0k",
                "fullName": "Paul WholyMoly",
                "role": "MODERATOR",
                "isPresenter": "true",
                "isListeningOnly": "true",
                "hasJoinedVoice": "false",
                "hasVideo": "false",
                "clientType": "HTML5"
            }
        },
        "metadata": {
            "bbb-context-id": "6",
            "bbb-origin-server-name": "neutral.ddev.site",
            "bbb-origin": "Drupal",
            "bbb-context": "test",
            "bbb-context-uuid": "3b59ca74-65d5-4a08-8a6d-2d21f2aff154",
            "endcallbackurl": "https%3A%2F%2Fneutral.ddev.site%3A4441%2Fbigbluebutton%2Fmeeting-end%2F3b59ca74-65d5-4a08-8a6d-2d21f2aff154%2Fnode"
        },
        "isBreakout": "false"
    },
    "drupal": {
        "entity_type_id": "node",
        "entity_id": "6",
        "entity_uuid": "3b59ca74-65d5-4a08-8a6d-2d21f2aff154"
    }
}

```
